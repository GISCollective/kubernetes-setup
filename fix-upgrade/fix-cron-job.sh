#/bin/bash

SECRET="sh.helm.release.v1.ogm.v16"
NAMESPACE="ogm"

UPDATE=$(kubectl get secret "${SECRET}" -n "${NAMESPACE}" -otemplate='{{.data.release |base64decode |base64decode }}'|gzip -d|sed 's#batch/v1beta1#batch/v1#'| gzip |base64 -w0 |base64 -w0)
kubectl patch secret "${SECRET}" -n "${NAMESPACE}" --patch="{\"data\": { \"release\": \"$UPDATE\" }}"
