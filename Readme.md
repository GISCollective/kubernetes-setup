## Helm usage

### To create a Helm package:
```
helm package giscollective
```
a .tgz file will be created.

### To see what default values are provided and what values need to be set when installing:

```
helm show values ./giscollective-0.1.0.tgz
```

### To install the package
Use the `install` command, and set some variable values, depending on the client for which we make the installation. Some examples:

```
helm install --namespace giscollective app ./giscollective-0.1.0.tgz --set serviceName="GISCollective",clientName=app,domainName="app.giscollective.com"
```

```
helm install --namespace ogm ogm ./giscollective-0.1.0.tgz --set serviceName="OpenGreenMap",clientName=ogm,domainName="new.opengreenmap.org"
```

### To upgrade the package

```
helm upgrade --namespace giscollective app ./giscollective-0.9.0.tgz

helm upgrade --namespace ogm --reuse-values --set serviceName="GreenMap",clientName=ogm,domainName="greenmap.org" ogm ./giscollective-0.62.0.tgz
```

## Local setup with minikube


### Start mongo

```
sudo systemctl start mongod.service
```

### To be able to pull images

```
sudo docker login registry.gitlab.com

sudo cp /root/.docker/config.json /home/{username}/.docker

kubectl create secret generic regcred --from-file=.dockerconfigjson=/home/{username}/.docker/config.json --type=kubernetes.io/dockerconfigjson

kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "regcred"}]}'

kubectl create deployment gisc-api --image=registry.gitlab.com/giscollective/backend/ogm-server:master
```

### To set a host for the ingress service

```
minikube addons enable ingress

kubectl get ingress gisc-ingress
```

Wait for ADDRESS to be assigned, and add it to the corresponding host in  `/etc/hosts`

### For https connection, we need to generate a certificate for ingress

```
$ openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ${KEY_FILE} -out ${CERT_FILE} -subj "/CN=${HOST}/O=${HOST}"
```

```
kubectl create secret tls ${CERT_NAME} --key ${KEY_FILE} --cert ${CERT_FILE}
```

Then set it as a secret in the ingress yaml:

```
spec:
  tls:
    - hosts:
      - giscollective.ro
      secretName: ${CERT_NAME}
```

### To start minikube with kvm instead of virtualbox

```
minikube start --vm-driver kvm2
```

### Configure the DB to accept connections from the vm

To find the network used by minkube:
```
sudo virsh net-list
```

To find the ip of the network:
```
sudo virsh net-dumpxml minikube-net
```

```
sudo nano /etc/mongod.conf
```
And add the ip of the kvm vm in the net section here:
```
net:
  # Specify port number (27017 by default)
  port: 27017

  # Comma separated list of ip addresses to listen on (all local ips by default)
  bindIp: 127.0.0.1,::1,192.168.39.1
```

## Rancher setup

Configure a CA and generate certificates: https://helm.sh/docs/using_helm/#generating-certificate-authorities-and-certificates


## Other info

#### Issues with configuring Virtualbox on Fedora
https://www.virtualbox.org/ticket/11577?cversion=0&cnum_hist=16
